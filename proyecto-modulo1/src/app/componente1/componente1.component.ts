import { Component, OnInit, Input, HostBinding } from '@angular/core';
import { ListaM } from '../models/listam.model';

@Component({
  selector: 'app-componente1',
  templateUrl: './componente1.component.html',
  styleUrls: ['./componente1.component.css']
})
export class Componente1Component implements OnInit {
@Input() lista: ListaM;
@HostBinding('attr.class') cssClass = 'col-md-6';
  constructor() { 
 
  }

  ngOnInit(): void {
  }

}
