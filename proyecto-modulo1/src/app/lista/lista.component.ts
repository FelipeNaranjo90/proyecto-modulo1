import { Component, OnInit } from '@angular/core';
import { ListaM } from './../models/listam.model';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css']
})
export class ListaComponent implements OnInit {
  lista: ListaM[];
  constructor() {
    this.lista = [];
  }

  ngOnInit(): void {
  }

  guardar(nombre:string,descripcion:string):boolean {
    this.lista.push(new ListaM(nombre, descripcion));
    return false;

  }

}
